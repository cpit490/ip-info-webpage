## IP Info Webpage
A sample php webpage that returns IP geo information.

## Usage

```shell
# download Composer
curl -sS https://getcomposer.org/installer | php
# Install dependencies
php composer.phar install
# Run the webpage
php -S localhost:8080
```


