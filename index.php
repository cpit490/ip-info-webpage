<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>CPIT-490 | IP Info</title>
    <style>
        body {
            display: flex;
            justify-content: center;
            align-items: center;
            height: 100vh;
            background-color: #f4f4f4;
            font-family: Arial, sans-serif;
        }

        .info-box {
            width: 300px;
            padding: 20px;
            border-radius: 5px;
            background-color: #fff;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
            text-align: center;
        }

        h1 {
            color: #333;
        }

        h3 {
            color: #666;
        }
    </style>
</head>

<body>

    <div id="ip-info-box">
        <h1>IP Info</h1>
        <?php
        require_once 'vendor/autoload.php';

        use GeoIp2\Database\Reader;
        use GeoIp2\Exception\AddressNotFoundException;

        $countryDbReader = new Reader('./data/GeoLite2-Country.mmdb');
        $ip = $_SERVER['REMOTE_ADDR'];
        echo "<h3>IP Address: $ip</h3>";
        try {
            $record = $countryDbReader->country($ip);
            echo "<h3>Country: " . $record->country->name . "</h3>";
            echo "<h3>Country ISO Code: " . $record->country->isoCode . "</h3>";
        } catch (AddressNotFoundException $e) {
            echo "<h3>Country: Unknown</h3>";
            echo "<h3>Country ISO Code: Unknown</h3>";
        }
        ?>
    </div>
</body>

</html>